﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yslnr02.Models;

namespace yslnr02.Factories
{
    class ItemCategoryFactory
    {
        private static Dictionary<String, List<Category>> categoryCache = new Dictionary<string, List<Category>>();

        public static List<Category> GetCategories(string learnItemName)
        {
            if (!LearnItem.Exists(learnItemName)) 
                return new List<Category>();

            if (!categoryCache.ContainsKey(learnItemName))
                categoryCache.Add(learnItemName, FetchLearnItemCategories(learnItemName));

            return categoryCache[learnItemName];
        }

        private static List<Category> FetchLearnItemCategories(string learnItemName)
        {
            return Category.GetLearnItemCategories(learnItemName);
        }
    }
}
