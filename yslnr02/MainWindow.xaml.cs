﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using yslnr02.data.db;
using yslnr02.Factories;
using yslnr02.Models;
using yslnr02.Views;

namespace yslnr02
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _currentItemCategoryName;
        private Category _currentCategory;

        public MainWindow()
        {
            InitializeComponent();
            var menu = new MenuControl();
            BodyControl.Content = menu.Content;
        }

        public void LoadCategories(string content)
        {
            _currentItemCategoryName = content;
            if (LearnItem.GetItem(content) == null)
                ShowItemNotFoundErrorDialog(content);

            var categories = ItemCategoryFactory.GetCategories(content);
            if (categories.Count < 1)
                ShowNoCategoriesErrorDialogue(content);
            else
                ShowItemCategories(content, categories);
        }

        private void ShowNoCategoriesErrorDialogue(string itemName)
        {
            var errorDialogue = new ErrorDialog
            {
                ErrorMessage = {Content = "No " + itemName + " Categories available"}
            };
            errorDialogue.Show();
            errorDialogue.Focus();
        }

        private void ShowItemCategories(string learnItemName, List<Category> categories)
        {
            _currentItemCategoryName = learnItemName;
            if (categories.Count == 1)
                ShowSelectItemNumberWindow(learnItemName, categories);
            else
            {
                var categorySelect = new CategorySelect(categories) {ItemName = {Content = learnItemName + " Categories"}};
                categorySelect.Show();
                categorySelect.Focus();
                Application.Current.MainWindow.Visibility = Visibility.Hidden;
            }
        }

        private void ShowSelectItemNumberWindow(string learnItemName, List<Category> categories)
        {
            throw new NotImplementedException();
        }

        private void ShowItemNotFoundErrorDialog(string content)
        {
            throw new NotImplementedException();
        }

        public void LoadCategoryItemCountSelector(Category itemCategory)
        {
            _currentCategory = itemCategory;
            var selector = new ItemCountSelector(_currentItemCategoryName + "s", 5);
            selector.Focus();
            selector.Show();
        }

        public void StartLearning(int numOfItems)
        {
            throw new NotImplementedException();
        }
    }
}
