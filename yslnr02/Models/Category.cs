﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yslnr02.data.db;

namespace yslnr02.Models
{
    public class Category
    {
        private List<Category> _subCategories;
        private readonly int _id;
        private readonly string _parentId, _avatarId, _learnItemId, _name;
        private string _imageSource;

        public Category(int id, string name, string avatarId, string parentId, string learnItemId)
        {
            _id = id;
            _parentId = parentId;
            _name = name;
            _avatarId = avatarId;
            _learnItemId = learnItemId;
            setImageSource();
        }

        private void setImageSource()
        {
            _imageSource = "../data/media/no-image.jpg";
            if (_avatarId != "NULL")
                throw new NotImplementedException();

        }

        public static List<Category> GetLearnItemCategories(string learnItemName)
        {
            var itemId = LearnItem.GetItem(learnItemName).GetID();
            var categories = FetchByQuery("SELECT * FROM category WHERE learn_item_id='" + itemId + "' and parent_category_id='NULL'");
            foreach (var category in categories)
                category.LoadSubCategories(itemId);
            return categories;
        }

        private void LoadSubCategories(int itemId)
        {
            _subCategories = FetchByQuery("SELECT * FROM category WHERE learn_item_id='" + itemId + "' and parent_category_id='" + _parentId + "'");
        }


        private static List<Category> FetchByQuery(string query)
        {
            var categories = new List<Category>();
            var dataReader = ConnectionManager.ExecuteSqlQuery(query);
            while (dataReader.Read())
                categories.Add(
                    new Category(dataReader.GetInt32(0), dataReader.GetString(1), 
                        dataReader.GetString(2), dataReader.GetString(3), dataReader.GetString(4)));
            dataReader.Close();
            return categories;
        }

        public string GetName()
        {
            return _name;
        }

        public string GetSource()
        {
            return _imageSource;
        }
    }
}
