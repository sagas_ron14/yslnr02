﻿using System.Collections.Generic;
using System.Linq;
using yslnr02.data.db;

namespace yslnr02.Models
{
    class LearnItem
    {
        public static string ElementStyle = "{DynamicResource ItemButton01}";
        private static List<LearnItem> _learnItems;
        private readonly string _margin, _name;
        private readonly int _id;

        private LearnItem(int id, string name, string margin)
        {
            _margin = margin;
            _name = name;
            _id = id;
        }

        public string GetName()
        {
            return _name;
        }

        public string GetMargin()
        {
            return _margin;
        }

        public static List<LearnItem> GetAll()
        {
            if (_learnItems == null)
                UpdateItemList();
            return _learnItems;
        }

        private static void UpdateItemList()
        {
            _learnItems = new List<LearnItem>();

            var dataReader = ConnectionManager.ExecuteSqlQuery("SELECT * FROM learn_item");
            while (dataReader.Read())
                _learnItems.Add(new LearnItem(dataReader.GetInt32(0), dataReader.GetString(1), dataReader.GetString(2)));
            dataReader.Close();
        }

        public static LearnItem GetItem(string itemName)
        {
            var allItems = GetAll();
            return allItems.FirstOrDefault(learnItem => learnItem.GetName().Equals(itemName));
        }

        public static bool Exists(string learnItemName)
        {
            return GetItem(learnItemName) != null;
        }

        public int GetID()
        {
            return _id;
        }
    }
}
