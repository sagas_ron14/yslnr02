﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using yslnr02.Models;

namespace yslnr02.Views
{
    /// <summary>
    /// Interaction logic for CategorySelect.xaml
    /// </summary>
    public partial class CategorySelect : Window
    {
        private Dictionary<String, Category> _itemCategories;

        public CategorySelect(IEnumerable<Category> categories)
        {
            LoadCategories(categories);
            InitializeComponent();
            DrawCategoryButtons();
        }

        private void LoadCategories(IEnumerable<Category> categories)
        {
            _itemCategories = new Dictionary<string, Category>();
            foreach (var category in categories)
                _itemCategories.Add(category.GetName(), category);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Show();
            Application.Current.MainWindow.Focus();
            this.Close();
        }

        private void DrawCategoryButtons()
        {
            var margins = new[] {10, 10, 412, 46};
            var i = 1; 
            foreach (var category in _itemCategories.Values)
                DrawButton(category.GetSource(), category.GetName(), getMargin(margins, i++));

        }

        private string getMargin(IList<int> margins, int i)
        {
            var marginString = "" + margins[0] + "," + margins[1] + "," + margins[2] + "," + margins[3];

            if (i%5 == 0)
            {
                margins[1] += 320;
                margins[2] = 412;
            }
            else
                margins[2] -= 320;

            return marginString;
        }

        private void DrawButton(string categoryImageSource, string categoryName, String marginDimensions)
        {
            var stringReader = new StringReader(
                        "<Button " +
                        "Name='catBtn_" + categoryName + "' " +
                        "xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' " +
                        "xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml' " +
                        "Width='150' Height='150' BorderThickness='0,5' Background='{x:Null}' BorderBrush='#FF149AB8' Margin='" + marginDimensions + "'>" +
				            "<StackPanel>" +
                                "<Image Source='" + categoryImageSource + "' Height='115' VerticalAlignment='Top'/>" +
					            "<TextBlock Text='" + categoryName +"' HorizontalAlignment='Center' FontFamily='Tekton Pro' FontSize='18.667' Foreground='#FF008506'/>" +
				            "</StackPanel>" +
			            "</Button>"
                        );
            var xmlReader = XmlReader.Create(stringReader);
            var newButton = (Button)XamlReader.Load(xmlReader);
            CategoryGrid.Children.Add(newButton);
            newButton.Click += CategoryButton_Click;
        }

        private void CategoryButton_Click(object sender, RoutedEventArgs e)
        {
            var categoryName = (((Button)sender).Name).Substring(7);
            ((MainWindow)Application.Current.MainWindow).LoadCategoryItemCountSelector(_itemCategories[categoryName]);
            this.Close();
        }
    }
}
