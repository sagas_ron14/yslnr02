﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace yslnr02.Views
{
    /// <summary>
    /// Interaction logic for ItemCountSelectorDialogue.xaml
    /// </summary>
    public partial class ItemCountSelector : Window
    {
        public ItemCountSelector(string learnItemName, int count)
        {
            InitializeComponent();
            LearnItemName.Content = learnItemName;
            SelectorMessage.Content = "select number of " + learnItemName + " to show";
            DrawButtons(count);
        }

        public void DrawButtons(int number)
        {
            var margins = new[] { 120, 116, 0, 0 };
            for(var i = 0; i < number; i++)
                DrawButton((i + 1), getMargin(margins));
        }
        private string getMargin(IList<int> margins)
        {
            var marginString = "" + margins[0] + "," + margins[1] + "," + margins[2] + "," + margins[3];
             margins[0] += 80;

            return marginString;
        }

        private void DrawButton(int number, String marginDimensions)
        {
            var stringReader = new StringReader(
                        "<Button " +
                            "xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' " +
                            "xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml' " +
                            "Content='" + number + "' HorizontalAlignment='Left' Height='59.545' Margin='" + marginDimensions + "' " +
                            "Style='{DynamicResource NumSelectorBtnTemplate}' VerticalAlignment='Top' Width='59' " +
                            "Foreground='#FF4DD14D' FontFamily='SketchFlow Print' FontSize='29.333'/>"
                        );
            var xmlReader = XmlReader.Create(stringReader);
            var newButton = (Button)XamlReader.Load(xmlReader);
            CountSelectorGrid.Children.Add(newButton);
            newButton.Click += ItemNumberButton_Click;
        }

        private void ItemNumberButton_Click(object sender, RoutedEventArgs e)
        {
            var numOfItems = Int32.Parse((String)(((Button)sender).Content));
            ((MainWindow) Application.Current.MainWindow).StartLearning((int) numOfItems);
            this.Close();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ((MainWindow) Application.Current.MainWindow).Focus();
            ((MainWindow)Application.Current.MainWindow).Visibility = Visibility.Visible;
            this.Close();
        }
    }
}
