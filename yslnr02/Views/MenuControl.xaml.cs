﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml;
using yslnr02.data.db;
using yslnr02.Models;

namespace yslnr02.Views
{
	/// <summary>
	/// Interaction logic for MenuControl.xaml
	/// </summary>
	public partial class MenuControl : UserControl
	{
		public MenuControl()
		{
			this.InitializeComponent();
            GenerateItemButtons();
		}

        private void AddButtonToWindow(string name, string margin, string style)
        {
            var stringReader = new StringReader(@"<Button xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'
                        xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml' 
                        x:Name='" + name + "Button' Content='" + name + "' " +
                        "HorizontalAlignment='Center' Height='121' Margin='" + margin + "' Style='" + style + 
                        "' VerticalAlignment='Center' Width ='166'/>");
            var xmlReader = XmlReader.Create(stringReader);
            var newButton = (Button)XamlReader.Load(xmlReader);
            MenuGrid.Children.Add(newButton);
            newButton.Click += ItemButton_Click;
        }

        private void GenerateItemButtons()
        {
            var menuItems = LearnItem.GetAll();
            foreach (var item in menuItems)
                AddButtonToWindow(item.GetName(), item.GetMargin(), LearnItem.ElementStyle);
        }

	    private void ItemButton_Click(object sender, RoutedEventArgs e)
	    {
	        ((MainWindow) Application.Current.MainWindow).LoadCategories(((Button) sender).Content.ToString());
	    }
	}
}