﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yslnr02.data.db
{
    class ConnectionManager
    {
        private static ConnectionManager _manager;
        private SQLiteConnection _connection;

        private ConnectionManager(string dataSourceDbYslnrSqlite)
        {
            _connection = new SQLiteConnection(dataSourceDbYslnrSqlite);
        }

        private static SQLiteConnection GetSqLiteConnection()
        {
            if (_manager == null || _manager._connection == null ) 
                _manager = new ConnectionManager(@"Data Source=data\\db\\yslnr.sqlite");
            return _manager._connection;
        }

        public static SQLiteDataReader ExecuteSqlQuery(string query)
        {
            var connection = GetSqLiteConnection();
            try
            {
                connection.Open();
            }
            catch (InvalidOperationException)
            {
                _manager._connection = null;
                connection = GetSqLiteConnection();
                connection.Open();
            }
            var command = new SQLiteCommand(connection) {CommandText = query};
            var reader = command.ExecuteReader();
            return reader;
        }

        public static void ReleaseResources()
        {
            if (_manager._connection == null) return;
            _manager._connection.Close();
            _manager._connection.ReleaseMemory();
        }


    }
}
